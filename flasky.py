from flask import Flask, jsonify


flasky = Flask(__name__)


@flasky.route('/')
def home():
    return jsonify(data='Welcome to Flasky!')


if __name__ == '__main__':
    flasky.run()
